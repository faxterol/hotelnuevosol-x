<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Hospedaje en Tepic, Nayarit - Hotel Nuevo Sol</title>
	<link href="css/estilo.css" rel="stylesheet" type="text/css" />
	<link href="css/sexylightbox.css" rel="stylesheet" type="text/css" />
	<meta property="og:title" content="" /> 
	<meta property="og:type" content="company" /> 
	<meta property="og:url" content="http://hotelnuevosol.com" /> 
	<meta property="og:image" content="http://hotelnuevosol.com/logo.png" /> 
	<meta property="og:site_name" content="" /> 
	<script type="text/javascript" src="lib/jquery/jquery-1.4.2.min.js"></script> 
	<script type="text/javascript" src="lib/jquery/jquery.easing.1.3.js"></script>
	<script type="text/javascript" src="lib/jquery/sexylightbox.v2.3.jquery.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			SexyLightbox.initialize({color:'blanco', dir: 'sexyimages'});
			$("body > div > ul a").bind("click",function(){
				$("#bienvenida,#tarifas,#ubicacion,#contacto,#instalaciones").hide();
				$($(this).attr("href")).fadeIn("slow");
				return false;
			});
			
			$("#tarifas ol a").bind("click",function(){
				$("#tarifas > div.d > div").hide();
				$($(this).attr("href")).fadeIn("slow");
				return false;
			});
			
		});
	</script>
	<link href="favicon.ico" rel="shortcut icon" type="image/x-icon" />
</head>
<body>
	<h1><a title="Hotel Nuevo Sol - Tepic, Nayarit. Cuando visite Tepic... ¡Lo Esperamos!" href="index.php">Hotel Nuevo Sol - Tepic, Nayarit. Cuando visite Tepic... ¡Lo Esperamos!</a></h1>
	<div id="cuerpo">
		<div id="info">
			<p><strong>Servicios:</strong> Restaurant, Seguridad, Comfort, Higiene, Servicio de T.V. por Cable, Estacionamiento, Internet Inalámbrico, Servicio de Habitación, Moderna Sala de Juntas.</p>
			<div><img src="imgs/demoslide.jpg" /></div>
		</div>
		<ul>
			<li><a title="Tarifas - Hotel Nuevo Sol" href="#tarifas">Tarifas - Hotel Nuevo Sol</a></li>
			<li class="ub"><a title="Ubicación - Hotel Nuevo Sol" href="#ubicacion">Ubicación - Hotel Nuevo Sol</a></li>
			<li class="in"><a title="Instalaciones - Hotel Nuevo Sol" href="#instalaciones">Instalaciones - Hotel Nuevo Sol</a></li>
			<li class="ct"><a title="Contactos - Hotel Nuevo Sol" href="#contacto">Contactos - Hotel Nuevo Sol</a></li>
		</ul>
		
		<div id="bienvenida">
			<div class="i">
				<p class="eventos"><strong>Proximos Eventos:</strong> Feria Nacional Tepic 2012, Festival Amado Nervo, Expoboda, Expoauto, Semana de la Moto, Expo Emprende.</p>
				<div id="TT_RymEE1kkEpn9KcrKjfqzDzDjDvaK1nMlbYEdkcC5KEj"><h2><a href="http://www.tutiempo.net">Predicción meteorológica</a></h2><a href="http://www.tutiempo.net/tiempo/Tepic_Nay/MMEP.htm">El tiempo en Tepic, Nay.</a></div>
<script type="text/javascript" src="http://www.tutiempo.net/widget/eltiempo_RymEE1kkEpn9KcrKjfqzDzDjDvaK1nMlbYEdkcC5KEj"></script>
			</div>
			<div class="d">
				<h2>Mensaje de Bienvenida</h2>
				<p>Culturalmente la zona no tiene muchos lugares de interés, pero cuenta con grandes y verdes espacios recreativos para toda la familia como puede ser la nueva zona construida Ciudad de Las Artes, en parques recreativos se destacan La Loma, el Parque Ecológico, Alameda entre otros, mientras que en Museos u otros están Museo Cora, Ciudad de las Artes, y en deportes se destaca el Estadio Arena Cora.</p>
				<p>El templo de la cruz de zacate y su convento anexo, el Palacio Municipal y La Catedral la cual es única a nivel mundial que tiene sus dos torres de campanarios simetricas entre si ubicada frente a la plaza principal y el ayuntamiento. En la plaza principal o plaza de armas se aprecian edificios de arquitectura del siglo XIX y principios del siglo XX. Es de este punto donde se pueden encontrar los Turibuses; autobuses con un diseño clásico del siglo XIX que hacen recorridos en la ciudad y fuera de ella.</p>
				<p><img alt="Instalaciones del Hotel Nuevo Sol" src="imgs/instalaciones.png" /></p>
			</div>
		</div>
		<div id="tarifas" class="oculto">
			<div class="i">
				<h2>Tarífas</h2>
				<ol>
					<li><a title="Suite - Hotel Nuevo Sol" href="#tarifas-suite">Suite</a></li>
					<li><a class="hab1" title="Habitación con Aire Acondicionado y Pantalla - Hotel Nuevo Sol" href="#tarifas-habitaciontipo2">Habitación con Aire Acondicionado y Pantalla</a></li>
					<li><a class="hab2" title="Habitación con Aire Acondicionado Cama Matrimonial e Individual - Hotel Nuevo Sol" href="#tarifas-habitaciontipo3">Habitación con Aire Acondicionado Cama Matrimonial e Individual</a></li>
					<li><a class="hab3" title="Habitación con Aire Acondicionado y una Cama - Hotel Nuevo Sol" href="#tarifas-habitaciontipo4">Habitación con Aire Acondicionado y una Cama</a></li>
					<li><a class="hab4" title="Habitación Doble - Hotel Nuevo Sol" href="#tarifas-habitaciontipo5">Habitación Doble</a></li>
					<li><a class="hab5" title="Habitación con una Cama - Hotel Nuevo Sol" href="#tarifas-habitaciontipo6">Habitación con una Cama</a></li>
					<li><a class="hab6" title="Sala de Juntas - Hotel Nuevo Sol" href="#tarifas-habitaciontipo7">Sala de Juntas</a></li>
					<li><a class="hab7" title="Habitaciones Económicas - Hotel Nuevo Sol" href="#economicos">Habitaciones Económicas</a></li>
				</ol>
			</div>
			<div class="d">
				<div id="tarifas-suite">
					<h2>SUITE</h2>
					<p class="fprin"><img alt="Tarifa de Suites en Hotel Nuevo Sol" src="imgs/habitaciones/suites_1.jpg" /></p>
					<p class="desc">Amplia Suite para 1 o 2 Personas<br /><br />
					Servicios con que cuenta: 
					 Cama Queen Site, Televisión por Cable,
					Internet Inalámbrico, Teléfono, Closet,
					Aire Acondicionado, Tina de Baño, Baño.
					<br /><br />
					<strong>Tarifa por Día $600.00<br /> 
					Incluye IVA e ISH</strong>
					<br /><br />
					<span>Esta Tarifa Lleva de Cortesia un Desayuno Express</span></p>
					<p class="fsec">
						<img alt="Tarifa de Suites en Hotel Nuevo Sol" src="imgs/habitaciones/suites_2.jpg" />
						<img alt="Tarifa de Suites en Hotel Nuevo Sol" src="imgs/habitaciones/suites_3.jpg" />
						<img alt="Tarifa de Suites en Hotel Nuevo Sol" src="imgs/habitaciones/suites_4.jpg" />
						<img alt="Tarifa de Suites en Hotel Nuevo Sol" src="imgs/habitaciones/suites_5.jpg" />
					</p>
				</div>
				<div id="tarifas-habitaciontipo2" class="oculto">
					<h2>HABITACION CON AIRE ACONDICIONADO Y PANTALLA</h2>
					<p class="fprin"><img alt="Tarifa de Habitación con Aire Acondicionado y Pantalla en Hotel Nuevo Sol" src="imgs/habitaciones/hab2_1.jpg" /></p>
					<p class="desc">
						Habitación para 1 o 2 Personas
						<br /><br />
						Servicios con que cuenta:
						Cama Matrimonial, Pantalla de Lcd por Cable, Teléfono, Internet Inalámbrico, Aire Acondicionado Closet, Baño.
						<br /><br />
						<strong>Tarifa por Día <br />
						1 persona $430.00<br />
						2 personas $500.00 <br />
						Incluye IVA e ISH</strong>
						<br /><br />
						<span>Esta Tarifa Lleva de Cortesía un Desayuno Express</span></p>
					<p class="fsec">
						<img alt="Tarifa de Habitación con Aire Acondicionado y Pantalla en Hotel Nuevo Sol" src="imgs/habitaciones/hab2_2.jpg" />
						<img alt="Tarifa de Habitación con Aire Acondicionado y Pantalla en Hotel Nuevo Sol" src="imgs/habitaciones/hab2_3.jpg" />
						<img alt="Tarifa de Habitación con Aire Acondicionado y Pantalla en Hotel Nuevo Sol" src="imgs/habitaciones/hab2_4.jpg" />
						<img alt="Tarifa de Habitación con Aire Acondicionado y Pantalla en Hotel Nuevo Sol" src="imgs/habitaciones/hab2_5.jpg" />
						<img alt="Tarifa de Habitación con Aire Acondicionado y Pantalla en Hotel Nuevo Sol" src="imgs/habitaciones/hab2_6.jpg" />
					</p>
				</div>
				<div id="tarifas-habitaciontipo3" style="font-size: 0.85em;" class="oculto">
					<h2>HABITACION CON AIRE ACONDICIONADO CON CAMA MATRIMONIAL E INDIVIDUAL</h2>
					<p class="fprin"><img alt="Tarifa de Habitación con Aire Acondicionado con Cama Matrimonial e Individual en Hotel Nuevo Sol" src="imgs/habitaciones/hab3_1.jpg" /></p>
					<p class="desc">Habitación para 2 o 3 Personas<br />
					
					Servicios con que cuenta:
					Cama Matrimonial e Individual,  Televisión por Cable, Teléfono, Internet Inalámbrico, 
					Aire Acondicionado, Closet, Baño.<br /><br />
					
					<strong>Tarifa por Día <br />
					2 persona $520.00<br />
					3 personas $590.00 <br />
					Persona adicional <br />
					Adulto $70.00 y Menor $35.00<br />
					Incluye IVA e ISH</strong><br /><br />
					<span>Esta Tarifa Lleva de Cortesia un Desayuno Express</span></p>
					<p class="fsec">
						<img alt="Tarifa de Habitación con Aire Acondicionado con Cama Matrimonial e Individual en Hotel Nuevo Sol" src="imgs/habitaciones/hab3_2.jpg" />
						<img alt="Tarifa de Habitación con Aire Acondicionado con Cama Matrimonial e Individual en Hotel Nuevo Sol" src="imgs/habitaciones/hab3_3.jpg" />
						<img alt="Tarifa de Habitación con Aire Acondicionado con Cama Matrimonial e Individual en Hotel Nuevo Sol" src="imgs/habitaciones/hab3_4.jpg" />
					</p>
				</div>
				<div id="tarifas-habitaciontipo4" class="oculto">
					<h2>HABITACION CON AIRE ACONDICIONADO Y UNA CAMA</h2>
					<p class="fprin"><img alt="Tarifa de Habitación con Aire Acondicionado y Una Cama en Hotel Nuevo Sol" src="imgs/habitaciones/hab4_1.jpg" /></p>
					<p class="desc">Habitación para 1 o 2 Personas<br /><br />
						Servicios con que cuenta:<br />
						Cama Individual, Televisión por Cable, Teléfono, Internet Inalámbrico, 
						Aire Acondicionado, Closet, Baño.
						<br /><br />
						<strong>Tarifa por Día <br />
						2 persona $380.00<br />
						3 personas $500.00 <br />
						Incluye IVA Y ISH</strong><br /><br />
						<span>Esta Tarifa Lleva de Cortesía un Desayuno Express</span></p>
					<p class="fsec" style="text-align: right">
						<img alt="Tarifa de Habitación con Aire Acondicionado y Una Cama en Hotel Nuevo Sol" src="imgs/habitaciones/hab4_2.jpg" />
						<img alt="Tarifa de Habitación con Aire Acondicionado y Una Cama en Hotel Nuevo Sol" src="imgs/habitaciones/hab4_3.jpg" />
					</p>
				</div>
				<div id="tarifas-habitaciontipo5" class="oculto">
					<h2>HABITACION DOBLE</h2>
					<p class="fprin"><img alt="Tarifa de Habitación Doble en Hotel Nuevo Sol" src="imgs/habitaciones/hab5_1.jpg" /></p>
					<p class="desc">
						Habitación para 1 o 4 Personas
						<br />
						Servicios con que cuenta:
						2 Camas Matrimoniales, Televisión por Cable, Teléfono, Internet Inalámbrico, 
						Ventilador de Techo, Closet, Baño.
						<br /><br />
						<strong>Tarifa por Día <br />
						1 o 2 persona $410.00<br />
						3 personas $510.00<br />
						4 Personas $610.00 <br />
						Incluye IVA e ISH</strong>
						<br /><br />
						<span>Esta Tarifa Lleva de Cortesía un Desayuno Express</span></p>
					<p class="fsec">
						<img alt="Tarifa de Habitación Doble en Hotel Nuevo Sol" src="imgs/habitaciones/hab5_2.jpg" />
						<img alt="Tarifa de Habitación Doble en Hotel Nuevo Sol" src="imgs/habitaciones/hab5_3.jpg" />
						<img alt="Tarifa de Habitación Doble en Hotel Nuevo Sol" src="imgs/habitaciones/hab5_4.jpg" />
					</p>
				</div>
				<div id="tarifas-habitaciontipo6" class="oculto">
					<h2>HABITACION CON UNA CAMA</h2>
					<p class="fprin"><img alt="Tarifa de Habitación con una Cama en Hotel Nuevo Sol" src="imgs/habitaciones/hab6_1.jpg" /></p>
					<p class="desc">
						Habitación para 1 o 2 Personas
						<br /><br />
						Servicios con que cuenta:
						Cama Matrimonial, Televisión por Cable, Teléfono, Internet Inalámbrico, 
						Aire Acondicionado, Closet, Baño.
						<br /><br />
						<strong>Tarifa por Día <br />
						2 persona $300.00<br />
						3 personas $510.00 <br />
						Incluye IVA e ISH</strong>
						<br /><br />
						<span>Esta Tarifa Lleva de Cortesía un Desayuno Express</span></p>
					<p class="fsec">
						<img alt="Tarifa de Habitación con una Cama en Hotel Nuevo Sol" src="imgs/habitaciones/hab6_2.jpg" />
						<img alt="Tarifa de Habitación con una Cama en Hotel Nuevo Sol" src="imgs/habitaciones/hab6_3.jpg" />
						<img alt="Tarifa de Habitación con una Cama en Hotel Nuevo Sol" src="imgs/habitaciones/hab6_4.jpg" />
					</p>
				</div>
				<div id="tarifas-habitaciontipo7" class="oculto">
					<h2>MODERNA SALA DE JUNTAS</h2>
					<p class="fprin"><img alt="Tarifa de Sala de Juntas en Hotel Nuevo Sol" src="imgs/habitaciones/hab7_1.jpg" /></p>
					<p class="desc">Servicios con que cuenta:<br />
						Pantalla de Lcd HD de 40”, Teléfono,
						Sistema de Audio y Video de Alta Definición  
						Internet Inalámbrico, Mesa de Trabajo, Escritorio, Sillas Secretariales 
						Area de Cafetería, 
						Aire Acondicionado, Baño.<br /><br />
						
						<strong>Tarifa por Día $400.00<br />
						Incluye IVA e ISH</strong><br /><br />
						
						<span>Pregunta por nuestros Paquetes 
						de Servicio de Coffe Break</span></p>
					<p class="fsec">
						<img alt="Tarifa de Sala de Juntas en Hotel Nuevo Sol" src="imgs/habitaciones/hab7_2.jpg" />
						<img alt="Tarifa de Sala de Juntas en Hotel Nuevo Sol" src="imgs/habitaciones/hab7_3.jpg" />
						<img alt="Tarifa de Sala de Juntas en Hotel Nuevo Sol" src="imgs/habitaciones/hab7_4.jpg" />
						<img alt="Tarifa de Sala de Juntas en Hotel Nuevo Sol" src="imgs/habitaciones/hab7_5.jpg" />
						<img alt="Tarifa de Sala de Juntas en Hotel Nuevo Sol" src="imgs/habitaciones/hab7_6.jpg" />
					</p>
				</div>
				<div id="economicos" class="oculto">
					<h2>HABITACIONES ECONOMICAS</h2>
					<div>
						<h2>HOTEL ALEX</h2>
						<p><img alt="Hotel Alex, habitaciones económicos en Tepic Nayarit" src="imgs/hotelalex.jpg" /> Habitación con Cama Matrimonial, Televisión por Cable, Teléfono, Closet, Baño.</p>
						<p><strong>Tarifas Desde<br /> $140.00<br /> Incluye IVA Y ISH</strong></p>
					</div>
					<div>
						<h2>HOTEL MERIDA</h2>
						<p><img alt="Hotel Merida, habitaciones económicos en Tepic Nayarit" src="imgs/hotelmerida.jpg" /> Habitación con Cama Matrimonial, Televisión por Cable, Teléfono, Closet, Baño.</p>
						<p><strong>Tarifas Desde<br /> $160.00<br /> Incluye IVA Y ISH</strong></p>
					</div>
				</div>
			</div>
		</div>
		<div id="ubicacion" class="oculto">
			<h2>Ubicación</h2>
			<div style="width: 722px;height:298px; margin: 15px auto;"><iframe width="722" height="298" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps/ms?msa=0&amp;msid=200000629793605751827.0004b9e35beddf7a71df6&amp;ie=UTF8&amp;t=m&amp;ll=21.488163,-104.8895&amp;spn=0.002975,0.007735&amp;z=17&amp;output=embed"></iframe></div>
			<p>Domicilio<br />
				Av. de la Cultura no. 40 Cd. del Valle c.p. 63157<br />
				Tepic, Nayarit; Mexico.</p>
		</div>
		<div id="contacto" class="oculto">
			<div class="i">
				<p><img alt="Contactanos - Hotel Nuevo Sol" src="imgs/imgcontacto.jpg" /></p>
			</div>
			<div class="d">
				<h2>Contactos</h2>
				<p><span>Teléfono</span> <strong>311.214.0290</strong> <span>Fax</span> <strong>311.214.2413</strong> <span>E-mail</span> <strong>reservaciones@hotelnuevosol.com</strong> <span>Domicilio</span> Av. de la Cultura no. 40 <br />Cd. del Valle c.p. 63157<br />Tepic, Nayarit; Mexico.</p>
			</div>
		</div>
		
		<div id="instalaciones" class="oculto">
			<h2>Instalaciones</h2>
			<p>
				<?php 
				$d = dir("fotos/minis");
				while($a = $d->read()){
					if($a!="." && $a!=".."){
						echo '<a rel="sexylightbox[hns]" title="Ver fotografía de las instalaciones" href="fotos/bigs/'.$a.'"><img alt="Fotografia de las instalaciones del Hotel Nuevo Sol" src="fotos/minis/'.$a.'" /></a>';
					}
				}
				$d->close();
				?>
			</p>
		</div>
	</div>
</body>
</html>